<?php

namespace Kfilipowski\Paginator;

class Item implements ItemInterface
{
    /** @var string $link */
    private $link;

    /** @var string $label */
    private $label;

    /** @var string $title */
    private $title;

    /**
     * @param string $link
     * @param string $label
     * @param string $title
     */
    public function __construct(string $link, string $label, string $title)
    {
        $this->link  = $link;
        $this->label = $label;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}
