<?php

namespace Kfilipowski\Paginator\Provider;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DoctrineORMProvider extends AbstractProvider
{
    /** @var QueryBuilder */
    private $qb;

    /**
     * @param QueryBuilder $qb
     */
    public function __construct(QueryBuilder $qb)
    {
        $this->qb = $qb;
    }

    /**
     * @param int $currentPage
     * @param int $itemsPerPage
     * @return array
     */
    public function prepare(int $currentPage, int $itemsPerPage): array
    {
        $this->initRangeTo($itemsPerPage);
        $this->initRangeFrom($itemsPerPage, $currentPage);

        $this->qb->setFirstResult($this->rangeFrom())->setMaxResults($this->rangeTo());
        $data = new Paginator($this->qb, true);

        $this->itemsCount = $data->count();
        $this->pagesCount = intval(ceil($this->itemsCount / $itemsPerPage));

        return (array) $data->getIterator();
    }
}
