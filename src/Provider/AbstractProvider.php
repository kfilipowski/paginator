<?php

namespace Kfilipowski\Paginator\Provider;

abstract class AbstractProvider
{
    /** @var int */
    protected $pagesCount;

    /** @var int */
    protected $itemsCount;

    /** @var int */
    protected $rangeTo;

    /** @var int */
    protected $rangeFrom;

    /**
     * @param int $currentPage
     * @param int $itemsPerPage
     * @return array
     */
    abstract function prepare(int $currentPage, int $itemsPerPage): array;

    /**
     * @return int
     */
    public function getPagesCount(): int
    {
        return $this->pagesCount;
    }

    /**
     * @return int
     */
    public function getItemsCount(): int
    {
        return $this->itemsCount;
    }

    /**
     * @return int
     */
    protected function rangeTo()
    {
        return $this->rangeTo;
    }

    /**
     * @return int
     */
    protected function rangeFrom()
    {
        return $this->rangeFrom;
    }

    /**
     * @param int $itemsPerPage
     */
    protected function initRangeTo(int $itemsPerPage)
    {
        $this->rangeTo = $itemsPerPage;
    }

    /**
     * @param int $itemsPerPage
     * @param int $currentPage
     */
    protected function initRangeFrom(int $itemsPerPage, int $currentPage)
    {
        $this->rangeFrom = $currentPage > 1 ? ($currentPage - 1) * $itemsPerPage : 0;
    }
}
