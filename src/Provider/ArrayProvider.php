<?php

namespace Kfilipowski\Paginator\Provider;

class ArrayProvider extends AbstractProvider
{
    /** @var array */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param int $currentPage
     * @param int $itemsPerPage
     * @return array
     */
    public function prepare(int $currentPage, int $itemsPerPage): array
    {
        $this->initRangeTo($itemsPerPage);
        $this->initRangeFrom($itemsPerPage, $currentPage);

        $this->itemsCount = count($this->data);
        $this->pagesCount = intval(ceil($this->itemsCount / $itemsPerPage));

        return array_slice($this->data, $this->rangeFrom(), $this->rangeTo());
    }
}
