<?php

namespace Kfilipowski\Paginator\Provider;

use Doctrine\ODM\MongoDB\Query\Builder;

class DoctrineODMProvider extends AbstractProvider
{
    /** @var Builder */
    private $qb;

    /**
     * @param Builder $qb
     */
    public function __construct(Builder $qb)
    {
        $this->qb = $qb;
    }

    /**
     * @param int $currentPage
     * @param int $itemsPerPage
     * @return array
     */
    public function prepare(int $currentPage, int $itemsPerPage): array
    {
        $this->initRangeTo($itemsPerPage);
        $this->initRangeFrom($itemsPerPage, $currentPage);

        $this->itemsCount = $this->queryBuilder->getQuery()->execute()->count();
        $this->pagesCount = intval(ceil($this->itemsCount / $itemsPerPage));

        $this->qb->skip($this->rangeFrom())->limit($this->rangeTo());
        $data = $this->qb->getQuery()->execute();

        return (array) $data->getIterator();
    }
}
