<?php

namespace Kfilipowski\Paginator;

interface ItemInterface
{
    /**
     * @return string
     */
    public function getLink(): string;

    /**
     * @return string
     */
    public function getLabel(): string;

    /**
     * @return string
     */
    public function getTitle(): string;
}
