<?php

namespace Kfilipowski\Paginator;

use Kfilipowski\Paginator\View\ViewInterface;

interface PaginatorInterface
{
    /**
     * @param int $currentPage
     * @param int $itemsPerPage
     */
    public function paginate(int $currentPage, int $itemsPerPage);

    /**
     * @return array
     */
    public function getData();

    /**
     * @return ViewInterface
     */
    public function getView(): ViewInterface;

    /**
     * @return int
     */
    public function getItemsCount(): int;

    /**
     * @return int
     */
    public function getPagesCount(): int;

    /**
     * @return int
     */
    public function getCurrentPage(): int;

    /**
     * @return int
     */
    public function getItemsPerPage(): int;
}
