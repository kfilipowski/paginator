<?php

namespace Kfilipowski\Paginator\View;

use Kfilipowski\Paginator\ItemInterface;

interface ViewInterface
{
    /**
     * @return void
     */
    public function create();

    /**
     * @return array
     */
    public function getItems(): array;

    /**
     * @return bool
     */
    public function hasPrevItem(): bool;

    /**
     * @return bool
     */
    public function hasNextItem(): bool;

    /**
     * @return ItemInterface|null
     */
    public function getPrevItem(): ?ItemInterface;

    /**
     * @return ItemInterface|null
     */
    public function getNextItem(): ?ItemInterface;

    /**
     * @param int $itemsCount
     */
    public function setItemsCount(int $itemsCount);

    /**
     * @param int $currentPage
     */
    public function setCurrentPage(int $currentPage);

    /**
     * @param int $pagesCount
     */
    public function setPagesCount(int $pagesCount);
}
