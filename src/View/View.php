<?php

namespace Kfilipowski\Paginator\View;

use Kfilipowski\Paginator\ItemInterface;

class View implements ViewInterface
{
    /** @var int */
    private $itemsCount;

    /** @var int */
    private $currentPage;

    /** @var int */
    private $pagesCount;

    /** @var array */
    private $items;

    /** @var \Closure */
    private $callback;

    /**
     * @param \Closure $callback
     */
    public function __construct(\Closure $callback)
    {
        $this->items    = [];
        $this->callback = $callback;
    }

    /**
     * @throws \Exception
     * @return void
     */
    public function create()
    {
        for ($page = 1 ; $page <= $this->pagesCount ; $page++) {

            $item = $this->callback->__invoke($page);

            if (!$item instanceof ItemInterface) {
                throw new \Exception(sprintf('Item must implement %s.', ItemInterface::class));
            }
            $this->items[] = $item;
        }
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return bool
     */
    public function hasPrevItem(): bool
    {
        return $this->currentPage > 1;
    }

    /**
     * @return bool
     */
    public function hasNextItem(): bool
    {
        return $this->currentPage < $this->pagesCount;
    }

    /**
     * @return ItemInterface|null
     */
    public function getPrevItem(): ?ItemInterface
    {
        if ($this->hasPrevItem()) {
            return $this->items[$this->currentPage - 2];
        }
        return null;
    }

    /**
     * @return ItemInterface|null
     */
    public function getNextItem(): ?ItemInterface
    {
        if ($this->hasNextItem()) {
            return $this->items[$this->currentPage];
        }
        return null;
    }

    /**
     * @param int $itemsCount
     */
    public function setItemsCount(int $itemsCount)
    {
        $this->itemsCount = $itemsCount;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage(int $currentPage)
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @param int $pagesCount
     */
    public function setPagesCount(int $pagesCount)
    {
        $this->pagesCount = $pagesCount;
    }
}
