<?php

namespace Kfilipowski\Paginator;

use Kfilipowski\Paginator\View\ViewInterface;
use Kfilipowski\Paginator\Provider\AbstractProvider;

class Paginator implements PaginatorInterface
{
    /** @var int */
    private $currentPage;

    /** @var int */
    private $itemsPerPage;

    /** @var array */
    private $data;

    /** @var ViewInterface */
    private $view;

    /** @var AbstractProvider */
    private $provider;

    /**
     * @param AbstractProvider $provider
     * @param ViewInterface $view
     */
    public function __construct(AbstractProvider $provider, ViewInterface $view)
    {
        $this->data     = [];
        $this->view     = $view;
        $this->provider = $provider;

        $this->currentPage  = 0;
        $this->itemsPerPage = 0;
    }

    /**
     * @param int $currentPage
     * @param int $itemsPerPage
     * @throws \InvalidArgumentException
     */
    public function paginate(int $currentPage, int $itemsPerPage)
    {
        $this->currentPage  = $currentPage;
        $this->itemsPerPage = $itemsPerPage;

        $this->data = $this->provider->prepare($currentPage, $itemsPerPage);

        $this->checkCurrentPage($currentPage);

        $this->view->setCurrentPage($this->currentPage);
        $this->view->setItemsCount($this->provider->getItemsCount());
        $this->view->setPagesCount($this->provider->getPagesCount());

        $this->view->create();
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return ViewInterface
     */
    public function getView(): ViewInterface
    {
        return $this->view;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @return int
     */
    public function getItemsPerPage(): int
    {
        return $this->itemsPerPage;
    }

    /**
     * @return int
     */
    public function getItemsCount(): int
    {
        return $this->provider->getItemsCount();
    }

    /**
     * @return int
     */
    public function getPagesCount(): int
    {
        return $this->provider->getPagesCount();
    }

    /**
     * @param int $currentPage
     * @throws \InvalidArgumentException
     */
    private function checkCurrentPage(int $currentPage)
    {
        if (0 === $currentPage || $currentPage > $this->provider->getPagesCount()) {
            throw new \InvalidArgumentException('Current page value is invalid.');
        }
    }
}
