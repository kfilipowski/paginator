<?php

namespace Kfilipowski\Paginator\Test\Provider;

use PHPUnit\Framework\TestCase;
use Kfilipowski\Paginator\Provider\ArrayProvider;

class ArrayProviderTest extends TestCase
{
    /**
     * @dataProvider variantProvider
     */
    public function testBasic($currentPage, $itemsPerPage, $data, $expected)
    {
        $context = new ArrayProvider($data);
        $actual = $context->prepare($currentPage, $itemsPerPage);

        $this->assertSame($expected, $actual);
    }

    public function variantProvider()
    {
        $data = [
            ['id' => 1],
            ['id' => 2],
            ['id' => 3],
            ['id' => 4],
        ];

        return [
            [1, 2, $data, [['id' => 1], ['id' => 2]]],
            [2, 2, $data, [['id' => 3], ['id' => 4]]],
        ];
    }
}
