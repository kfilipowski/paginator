<?php

namespace My\Paginator\Test;

use PHPUnit\Framework\TestCase;
use Kfilipowski\Paginator\Paginator;
use Kfilipowski\Paginator\View\ViewInterface;
use Kfilipowski\Paginator\Provider\AbstractProvider;

class PaginatorTest extends TestCase
{
    public function testBasic()
    {
        $view     = $this->getViewMock();
        $provider = $this->getProviderMock();

        $context = new Paginator($provider, $view);
        $context->paginate(1, 10);

        $this->assertSame($context->getView(), $view);

        $this->assertSame(1, $context->getCurrentPage());
        $this->assertSame(4, $context->getPagesCount());

        $this->assertSame(40, $context->getItemsCount());
        $this->assertSame(10, $context->getItemsPerPage());
    }

    private function getViewMock()
    {
        $mock = $this->createMock(ViewInterface::class);
        return $mock;
    }

    private function getProviderMock()
    {
        $mock = $this->createMock(AbstractProvider::class);

        $mock->method('prepare')->willReturn(range(0, 9));
        $mock->method('getPagesCount')->willReturn(4);
        $mock->method('getItemsCount')->willReturn(40);

        return $mock;
    }
}
