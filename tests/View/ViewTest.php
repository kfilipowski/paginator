<?php

namespace My\Paginator\Test;

use PHPUnit\Framework\TestCase;
use Kfilipowski\Paginator\Item;
use Kfilipowski\Paginator\View\View;

class ViewTest extends TestCase
{
    public function testBasic()
    {
        $context = new View(function($page) {
            return $this->createItemMock($page);
        });

        $context->setItemsCount(90);
        $context->setCurrentPage(3);
        $context->setPagesCount(9);
        $context->create();

        $items = $context->getItems();

        $this->assertSame(9, count($items));
        $this->assertFalse(isset($items[10]));

        $this->assertSame('link1', $items[0]->getLink());
        $this->assertSame('link9', $items[8]->getLink());

        $this->assertSame('link2', $context->getPrevItem()->getLink());
        $this->assertSame('link4', $context->getNextItem()->getLink());
    }

    private function createItemMock($page)
    {
        $mock = $this->createMock(Item::class);
        $mock->method('getLink')->willReturn('link' . $page);
        return $mock;
    }
}
